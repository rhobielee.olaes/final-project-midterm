// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDJFzK1u1OPBKjb4TBhTg7CaIJo39sF574",
    authDomain: "ionic-login-61d83.firebaseapp.com",
    projectId: "ionic-login-61d83",
    storageBucket: "ionic-login-61d83.appspot.com",
    messagingSenderId: "22604530562",
    appId: "1:22604530562:web:e3b0ab144fb32f0df0c59b"
  }

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
