import { Component, OnInit } from '@angular/core';
import { FirebaseService } from '../services/firebase.service';

@Component({
  selector: 'app-feed',
  templateUrl: './feed.page.html',
  styleUrls: ['./feed.page.scss'],
})
export class FeedPage implements OnInit {
  searchTerm: string;
  postList = [];

  constructor(private firebaseService : FirebaseService) { }

  ngOnInit() {
    this.firebaseService.read_students().subscribe(data => {

      this.postList = data.map(e => {
        return {
          fullName: e.payload.doc.data()['fullName'],
          sideJob: e.payload.doc.data()['sideJob'],
          sideDesc: e.payload.doc.data()['sideDesc'],
          Address: e.payload.doc.data()['Address'],
          contactNumber: e.payload.doc.data()['contactNumber']
        };
      })
      console.log(this.postList);

    });
  }

}
