import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { AlertController } from '@ionic/angular';
interface AccountData {
  firstName: string;
  middleName: string;
  lastName: string;
  Gender: string;
  Age: number;
  Contact_Number: number;
  Address: string;
  
}

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  accountList = [];
  accountData: AccountData;
  accountForm: FormGroup;
  

  user ={
    email: '',
    password: ''
  }
  userDoc: any;

  constructor(
    private router: Router,
    public ngFireAuth: AngularFireAuth,
    private afStore: AngularFirestore,
    public fb: FormBuilder,
    public alert: AlertController,
    ) {
      this.accountData = {} as AccountData;
      
     }

  ngOnInit() {

    this.accountForm = this.fb.group({
      firstName: ['', [Validators.required]],
      middleName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      Gender: ['', [Validators.required]],
      Age: ['', [Validators.required]],
      contactNumber: ['', [Validators.required]],
      Address: ['', [Validators.required]],
      Email: ['', [Validators.required]],
      Password: ['', [Validators.required]],
      confirmPassword: ['', [Validators.required]]


    })

    // this.firebaseService.read_students().subscribe(data => {

    //   this.accountList = data.map(e => {
    //     return {
    //       id: e.payload.doc.id,
    //       isEdit: false,
    //       First_Name: e.payload.doc.data()['firstName'],
    //       Middle_Name: e.payload.doc.data()['middleName'],
    //       Last_Name: e.payload.doc.data()['lastName'],
    //       Gender: e.payload.doc.data()['Gender'],
    //       Age: e.payload.doc.data()[' Age'],
    //       Contact_Number: e.payload.doc.data()['contactNumber'],
    //       Address: e.payload.doc.data()['Address'],     
    //     };
    //   })
      
    //   console.log(this.accountList);

    // });

  }

  async showAlert(header: string, message: string){
    const alert = await this.alert.create({
      header,
      message,
      buttons: ["OK"]
    })

    await  alert.present()

  }
  async Register() {
    if (this.accountForm.value.Password != this.accountForm.value.confirmPassword){
      this.showAlert("Error", "Passwords don't match");
      return console.error('Password Does Not Match!!');
    }
    try{
      const user = await this.ngFireAuth.createUserWithEmailAndPassword(this.accountForm.value.Email,this.accountForm.value.Password);
      console.log(user);
      const UID = user.user.uid
      await user.user.sendEmailVerification();
      this.userDoc = this.afStore.doc<any>(`Students/${UID}`)
      this.userDoc.set(this.accountForm.value)
      
       if(user.user.email){
        this.accountForm.reset();
        this.showAlert("Success!", "Welcome aboard!");
         this.router.navigate(['/login']);
       }
      }catch(error) {
        console.dir(error);
        this.showAlert("Error!", error.message);
      }
       }
      
       Cancel(){
        this.router.navigate(['/login']);
       }

    

}
