import { Component, OnInit } from '@angular/core';
import { FirebaseService } from '../services/firebase.service';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

interface postData {
  fullName: string;
  sideJob: string;
  sideDesc: string;
  Address: string;
  contactNumber: number;
}

@Component({
  selector: 'app-uploader',
  templateUrl: './uploader.page.html',
  styleUrls: ['./uploader.page.scss'],
})
export class UploaderPage implements OnInit {
  user = {
    fullName:'',
    sideJob: '',
    sideDesc: '',
    Address: '',
    contactNumber: ''
  }
  postData: postData;
  postForm: FormGroup;

  constructor(
    private firebaseService: FirebaseService,
    public alert: AlertController,
    private afStore: AngularFirestore,
    private router: Router,
    public fb: FormBuilder
  ) {

   }

  ngOnInit() {
    this.postForm = this.fb.group({
      fullName: ['', [Validators.required]],
      sideJob:['', [Validators.required]],
      sideDesc: ['', [Validators.required]],
      Address:['', [Validators.required]],
      contactNumber: ['', [Validators.required]]
      
      
    })
    
  }
createPost(){
  try{

    const UID = this.afStore.createId();
    this.afStore.doc<any>(`POST/${UID}`).set({
      fullName: this.postForm.value['fullName'],
      sideJob: this.postForm.value['sideJob'],
      sideDesc: this.postForm.value['sideDesc'],
      Address: this.postForm.value['Address'],
      contactNumber: this.postForm.value['contactNumber'],
      Email: sessionStorage.getItem('email'),
      isEdit: false,
      id: UID
    }).then(resp => {
      this.postForm.reset();
    })
      .catch(error => {
        console.log(error);
      });
      this.showAlert("Success!", "Your post is Uploaded!", '/tabs');
      // this.router.navigate(['/tabs']);

  } catch(error) {
    console.dir(error);
     this.showsAlert("Error!", error.message);
}
}
async showAlert(header: string, message: string, path: string){
  const alert = await this.alert.create({
    header,
    message,
    buttons: 
    [
      {
        text: 'Okay',
        handler: () => 
        {
          this.router.navigate([path]);
        }
      },
      
    ]
  })

  await  alert.present()

}
async showsAlert(header: string, message: string){
  const alert = await this.alert.create({
    header,
    message,
    buttons: ['OK'] 
  })

  await  alert.present()

}
}
