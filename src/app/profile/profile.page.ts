import { Component, OnInit } from '@angular/core';
import { FirebaseService } from '../services/firebase.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  studentList = [];
  currentUser ;
  postUpload = [] ;
  userDoc: any;

  constructor(private firebaseService: FirebaseService,private fireStore: AngularFirestore, private router: Router) { }

  ngOnInit() {

  //  const UID = console.log(sessionStorage.getItem('uid'));
  const UID: string= sessionStorage.getItem('uid');
  const Lemail: string = sessionStorage.getItem('email')
  //  const res  = this.fireStore.doc(`Students/${UID}`);

  // const res  = this.fireStore.doc('Students' +'/' + UID);
  // console.log(res)

  //  res.get({
      
  //  })
    this.firebaseService.getCurrentUserInfo(UID).subscribe(data =>
    {
      this.currentUser = data;
      console.log(this.currentUser)
    })


      this.firebaseService.getPost(Lemail).subscribe(post =>
          {
            this.postUpload = post
            console.log(this.postUpload);
      
          });
            
    
  

    
  }
  UpdatePost(row,rowrandom){
    let record = {};
    record['fullName'] = row.EditFullName;
    record['sideJob'] = row.EditSideJob;
    record['sideDesc'] = row.EditSideDesc;
    record['Address'] = row.EditAddress;
    record['contactNumber'] = row.EditContactNumber;
    this.firebaseService.update_post(row.id, record);
    row.isEdit = false;


  }
RemovePost(uid){
  this.firebaseService.delete_post(uid);
}
EditPost(record) {
  record.isEdit = true;
  record.EditFullName = record.fullName;
  record.EditSideJob = record.sideJob;
  record.EditSideDesc = record.sideDesc;
  record.EditAddress = record.Address;
  record.EditContactNumber = record.contactNumber;
}

logOut(){
  this.router.navigate(['/login'])
}

  

}
