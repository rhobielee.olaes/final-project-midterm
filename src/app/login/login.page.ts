import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AngularFireAuth } from '@angular/fire/auth';
import { AlertController } from '@ionic/angular';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  user ={
    email: '',
    password: ''
  }

  constructor(private router: Router,public ngFireAuth: AngularFireAuth,public alert: AlertController,) {
    
   }

  ngOnInit() {
  }
  async LogMeIn() {
    try{
    const user = await this.ngFireAuth.signInWithEmailAndPassword(this.user.email,this.user.password);
    console.log(user);
    if(user.user.email){
      const UID = user.user.uid
      const Lemail = this.user.email
      sessionStorage.setItem('email',Lemail)
      sessionStorage.setItem("uid",UID)
      this.showAlert("Success!", "Welcome aboard!");
      this.router.navigate(['/tabs']);
    }
  }catch(error) {
    console.dir(error)
    this.showAlert("Error!", error.message)
  }
  }
  RegisterMe() {
    this.router.navigate(['/register']);
  }
  async showAlert(header: string, message: string){
    const alert = await this.alert.create({
      header,
      message,
      buttons: ["OK"]
    })
    await  alert.present()
  }
}
